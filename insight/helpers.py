from astropy.io import ascii
import numpy
import ast

def pythonize(expression):
    """
    Converts an expression to a Python type if possible.

    Will first attempt to convert numpy types to native Python equivalents.
    Then, it will evaluate the expression and return a Python data type, or
    a string if this fails.
    """
    if isinstance(expression, numpy.generic):
        expression = numpy.asscalar(expression)
    try:
        return ast.literal_eval(expression)
    except Exception:
        return expression

def parse_text_table(ascii_table):
    """
    Parse an ASCII formatted table and return a list of dicts - one per row.

    Uses the astropy package's 'io.ascii' module to get a data table from  an
    ASCII based one.

    Given the following table:

    | name    | favourite fruit | favourite vegetable |

    | Bob     | apples          | cucumber            |
    | Belinda | oranges         | lettuce             |
    | Baz     | fish            | fish                |

    This function will return the following list of dictionaries:
    [
    {"name": "Bob",
    "favourite fruit": "apples",
    "favourite vegetable": "cucumber" }
    {"name": "Belinda",
    "favourite fruit": "oranges",
    "favourite vegetable": "lettuce" }
    {"name": "Baz",
    "favourite fruit": "fish",
    "favourite vegetable": "fish" }
    ]
    """
    table = ascii.read(ascii_table, format='fixed_width', header_start=0)
    list_of_dicts = []
    for row in range(len(table)):
        rowdict = {}
        for column in table.colnames:
            value = pythonize(table.columns[column][row])
            rowdict[column] = value
        list_of_dicts.append(rowdict)
    return list_of_dicts
