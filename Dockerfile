FROM gcr.io/google_appengine/python

RUN virtualenv -p python3 /env
ENV PATH /env/bin:$PATH

RUN apt-get update -y && apt-get install --no-install-recommends -y -q tesseract-ocr

ADD requirements.txt /app/requirements.txt
RUN /env/bin/pip install --upgrade pip && /env/bin/pip install -r /app/requirements.txt
ADD . /app


CMD gunicorn -b :$PORT insight.wsgi
