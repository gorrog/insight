Feature: Launching a task after upload of an image

    In order to have invoice data available to display to the user
    and not need to wait for other invoices to be processed first
    As the API
    I want a task to process an invoice image to be launched whenever
    an invoice image is uploaded

Background:
    Given that we have a way of generating test images
        
Scenario: Create invoices with an image

    When we create <num_invoices> invoices with our test images
        Then <num_tasks> tasks will be launched
            And each invoice will have a status of 'Queued'
        
        | num_invoices | num_tasks |

        |      1       |     1     |
        |      2       |     2     |
        |      3       |     3     |
        |      30      |     30    |
