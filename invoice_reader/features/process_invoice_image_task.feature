Feature: Processing an invoice image with a task

    In order to have machine readable data about a scanned invoice
    As a user
    I want a task to process an invoice image and extract key information
    about it.

Background:
    Given that we have 2 sample invoice images
        And that the first invoice is numbered 1
        And that the second invoice is numbered 2
        And we have a third image containing random text
        And we have a fourth image containing no text
        And we have an invoice object for each image
        And we have a task that can process invoices
        
Scenario: Process an Invoice With a Task

    When we pass the id of invoice <invoice_number> to the task
        Then the invoice's billed_by_address field will have a value of <billed_by_address>
        And the invoice's billed_by_name field will have a value of <billed_by_name>
        And the invoice's billed_by_vat_no field will have a value of <billed_by_vat_no>
        And the invoice's billed_by_tel_no field will have a value of <billed_by_tel_no>
        And the invoice's billed_to_address field will have a value of <billed_to_address>
        And the invoice's billed_to_account_no field will have a value of <billed_to_account_no>
        And the invoice's billed_to_name field will have a value of <billed_to_name>
        And the invoice's date_invoiced field will have a value of <date_invoiced>
        And the invoice's invoice_total field will have a value of <invoice_total>
        And the invoice's vat_percentage field will have a value of <vat_percentage>
        And the invoice's status field will have a value of <status>

           | invoice_number | billed_by_address                        | billed_by_name    |   billed_by_vat_no | billed_by_tel_no  | billed_to_address                        | billed_to_account_no | billed_to_name    | date_invoiced     | invoice_total     | vat_percentage    | status    |
                                                                                                                                                                                                                                                                                      
           |       1        | Street No 20, Test Town, Test City, 2191 | Insight           |   0158653          | 010-012-6359      | Street Address, 42 Main Road, Brianville | TC-02545             | CL-02545          | 21/08/2016        | 2433.40           | 15                |    d      |
           |       2        | Street No 20, Test Town, Test City, 2191 | Insight           |   0158653          | 010-012-6359      | Street Address, 9 2nd Drive, Dave County | TC-02545             | Titan Core        | 21/08/2016        | 2433.40           | 15                |    d      |
           |       3        | Cannot find field                        | Cannot find field | Cannot find field  | Cannot find field | Cannot find field                        | Cannot find field    | Cannot find field | Cannot find field | Cannot find field | Cannot find field |    d      |
           |       4        |                                          |                   |                    |                   |                                          |                      |                   |                   |                   |                   |    f      |
