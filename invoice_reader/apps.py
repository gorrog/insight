from django.apps import AppConfig


class InvoiceReaderConfig(AppConfig):
    name = 'invoice_reader'
