from django.test import TestCase
from .sample_data import RAW_TEXT_FROM_IMAGE_1, SAMPLE_LINE
from ..tasks import get_lines_from_raw_text, get_vat_no, get_billed_by_email, \
    get_billed_by_address, get_billed_by_name, get_billed_by_tel_no, \
    get_billed_to_address, get_billed_to_account_no, get_billed_to_name, \
    get_date_invoiced, get_invoice_total, get_value_from_currency, \
    get_invoice_sub_total, get_vat_percentage, get_billed_items, \
    get_billed_item_description, get_billed_item_unit_cost

class GetLinesFromRawTextTestCase(TestCase):
    """ Tests the get_lines_from_raw_text() function in tasks.py ."""

    def setUp(self):
        self.lines = get_lines_from_raw_text(RAW_TEXT_FROM_IMAGE_1)

    def test_correct_number_of_lines(self):
        expected_result = 16
        actual_result = len(self.lines)
        error_message = ("get_lines_from_raw_text() returned an incorrect "
        "number of lines.")
        self.assertEqual(expected_result, actual_result, error_message)

    def test_all_lines_are_free_of_whitespace(self):
        for line in self.lines:
            expected_result = line.strip()
            actual_result = line
            error_message = ("get_lines_from_raw_text() did not remove leading "
            "and trailing whitespace correctly. Offending line is '{}'.")
            error_message = error_message.format(line)
            self.assertEqual(expected_result, actual_result, error_message)

    def test_no_empty_lines_returned(self):
        for line in self.lines:
            error_message = ("get_lines_from_raw_text() returned at least one "
            "empty line! Empty lines should have been ignored.")
            self.assertGreater(len(line), 0, error_message)


class GetVATNoTestCase(TestCase):
    """ Tests the get_vat_no() function in tasks.py ."""

    def setUp(self):
        self.lines = get_lines_from_raw_text(RAW_TEXT_FROM_IMAGE_1)

    def test_line_containing_number_returns_number(self):
        vat_no = get_vat_no(self.lines)
        expected = int
        actual = type(vat_no)
        message = "get_vat_no() did not return a number"
        self.assertEqual(expected, actual, message)

    def test_line_not_containing_number_returns_none(self):
        lines = self.lines
        lines[1] = "Some junk without a number at all"
        vat_no = get_vat_no(lines)
        expected = None
        actual = None
        message = "get_vat_no() returned something. Expected None"
        self.assertEqual(expected, actual, message)


class GetBilledByEmailTestCase(TestCase):
    """ Tests the get_billed_by_email() function in tasks.py ."""

    def setUp(self):
        self.lines = get_lines_from_raw_text(RAW_TEXT_FROM_IMAGE_1)

    def test_line_containing_email_returns_email(self):
        billed_by_email = get_billed_by_email(self.lines)
        expected = "sales@insightcom"
        actual = billed_by_email
        message = "Unexpected email returned"
        self.assertEqual(expected, actual, message)

    def test_line_not_containing_email_returns_none(self):
        lines = self.lines
        lines[0] = "Some junk without an email at all"
        vat_no = get_vat_no(lines)
        expected = None
        actual = None
        message = "get_billed_by_email() returned something. Expected None"
        self.assertEqual(expected, actual, message)


class GetBilledByAddressTestCase(TestCase):
    """ Tests the get_billed_by_address() function in tasks.py ."""

    def setUp(self):
        self.lines = get_lines_from_raw_text(RAW_TEXT_FROM_IMAGE_1)

    def test_lines_containing_address_returns_address(self):
        billed_by_address = get_billed_by_address(self.lines)
        expected = "Street No 20, aie ante) P1714"
        actual = billed_by_address
        message = "Unexpected billed by address returned"
        self.assertEqual(expected, actual, message)

    def test_lines_not_containing_first_line_address_returns_none(self):
        lines = self.lines
        lines[0] = "Insight sales@insightcom   " # nothing after email
        billed_by_address = get_billed_by_address(lines)
        expected = None
        actual = billed_by_address
        message = "get_billed_by_address() returned something. Expected None"
        self.assertEqual(expected, actual, message)

    def test_lines_not_containing_first_line_address_returns_none(self):
        lines = self.lines
        lines[1] = "Vat No: 0158653        " # nothing after vat no.
        billed_by_address = get_billed_by_address(lines)
        expected = None
        actual = billed_by_address
        message = "get_billed_by_address() returned something. Expected None"
        self.assertEqual(expected, actual, message)


class GetBilledByNameTestCase(TestCase):
    """ Tests the get_billed_by_name() function in tasks.py ."""

    def setUp(self):
        self.lines = get_lines_from_raw_text(RAW_TEXT_FROM_IMAGE_1)

    def test_lines_containing_billed_by_name_returns_billed_by_name(self):
        billed_by_name = get_billed_by_name(self.lines)
        expected = "Insight"
        actual = billed_by_name
        message = "Unexpected billed by name returned"
        self.assertEqual(expected, actual, message)

    def test_lines_not_containing_first_line_email_returns_none(self):
        lines = self.lines
        lines[0] = "Insight salesinsightcom  something " # no email
        billed_by_name = get_billed_by_name(lines)
        expected = None
        actual = billed_by_name
        message = "get_billed_by_address() returned something. Expected None"
        self.assertEqual(expected, actual, message)


class GetBilledByTelNoTestCase(TestCase):
    """ Tests the get_billed_by_tel_no() function in tasks.py ."""

    def setUp(self):
        self.lines = get_lines_from_raw_text(RAW_TEXT_FROM_IMAGE_1)

    def test_lines_containing_billed_by_tel_no_returns_billed_by_tel_no(self):
        billed_by_tel_no = get_billed_by_tel_no(self.lines)
        expected = "010-012-6359"
        actual = billed_by_tel_no
        message = "Unexpected billed by tel no returned"
        self.assertEqual(expected, actual, message)

    def test_lines_not_containing_tel_no_prefix_returns_none(self):
        lines = self.lines
        lines[2] = "iN VO o = Tell Nini: 010-012-6359 2191" # no 'Tel No:
        billed_by_tel_no = get_billed_by_tel_no(lines)
        expected = None
        actual = billed_by_tel_no
        message = "get_billed_by_tel_no() returned something. Expected None"
        self.assertEqual(expected, actual, message)


class GetBilledToAddressTestCase(TestCase):
    """ Tests the get_billed_to_address() function in tasks.py ."""

    def setUp(self):
        self.lines = get_lines_from_raw_text(RAW_TEXT_FROM_IMAGE_1)

    def test_lines_containing_billed_by_tel_no_returns_billed_by_tel_no(self):
        billed_to_address = get_billed_to_address(self.lines)
        expected = "42 Main Road, Brianville"
        actual = billed_to_address
        message = "Unexpected billed to address returned"
        self.assertEqual(expected, actual, message)

    def test_lines_not_containing_date_of_issue_returns_none(self):
        lines = self.lines
        lines[6] = "42 Main Road, Brianville Dater Oz Issuez" #no Date of Issue
        billed_to_address = get_billed_to_address(lines)
        expected = None
        actual = billed_to_address
        message = "get_billed_to_address() returned something. Expected None"
        self.assertEqual(expected, actual, message)


class GetBilledToAccountNoTestCase(TestCase):
    """ Tests the get_billed_to_account_no() function in tasks.py ."""

    def setUp(self):
        self.lines = get_lines_from_raw_text(RAW_TEXT_FROM_IMAGE_1)

    def test_lines_containing_billed_to_account_no_returns_billed_to_account_no(self):
        billed_to_account_no = get_billed_to_account_no(self.lines)
        expected = "TC-02545"
        actual = billed_to_account_no
        message = "Unexpected billed to account no returned"
        self.assertEqual(expected, actual, message)

    def test_lines_not_containing_billed_to_returns_none(self):
        lines = self.lines
        lines[3] = "Billee Tool TC-02545 Invoice Total" #no Billed To
        billed_to_account_no = get_billed_to_account_no(lines)
        expected = None
        actual = billed_to_account_no
        message = "get_billed_to_account_no() returned something. Expected None"
        self.assertEqual(expected, actual, message)

    def test_lines_not_containing_invoice_returns_none(self):
        lines = self.lines
        lines[3] = "Billed To TC-02545 Total" #no Invoice
        billed_to_account_no = get_billed_to_account_no(lines)
        expected = None
        actual = billed_to_account_no
        message = "get_billed_to_account_no() returned something. Expected None"
        self.assertEqual(expected, actual, message)

class GetBilledToNameTestCase(TestCase):
    """ Tests the get_billed_to_name() function in tasks.py ."""

    def setUp(self):
        self.lines = get_lines_from_raw_text(RAW_TEXT_FROM_IMAGE_1)

    def test_lines_containing_billed_to_account_no_returns_billed_to_account_no(self):
        billed_to_name = get_billed_to_name(self.lines)
        expected = "CL-02545"
        actual = billed_to_name
        message = "Unexpected billed to account no returned"
        self.assertEqual(expected, actual, message)

    def test_lines_not_containing_client_name_returns_none(self):
        lines = self.lines
        lines[4] = "Clien Namo CL-02545 R2433 40" #no Client Name
        billed_to_name = get_billed_to_name(lines)
        expected = None
        actual = billed_to_name
        message = "get_billed_to_name() returned something. Expected None"
        self.assertEqual(expected, actual, message)

class GetDateInvoicedTestCase(TestCase):
    """ Tests the get_date_invoiced() function in tasks.py ."""

    def setUp(self):
        self.lines = get_lines_from_raw_text(RAW_TEXT_FROM_IMAGE_1)

    def test_lines_containing_date_returns_date_invoiced(self):
        date_invoiced = get_date_invoiced(self.lines)
        expected = "21/08/2016"
        actual = date_invoiced
        message = "Unexpected date invoiced field returned"
        self.assertEqual(expected, actual, message)

    def test_lines_not_containing_correctly_formed_date_returns_none(self):
        lines = self.lines
        lines[7] = "12/34/4875:21" #Invalid date
        date_invoiced = get_date_invoiced(lines)
        expected = None
        actual = date_invoiced
        message = "get_date_invoiced() returned something. Expected None"
        self.assertEqual(expected, actual, message)


class GetInvoiceTotalTestCase(TestCase):
    """ Tests the get_invoice_total() function in tasks.py ."""

    def setUp(self):
        self.lines = get_lines_from_raw_text(RAW_TEXT_FROM_IMAGE_1)

    def test_lines_containing_date_returns_date_invoiced(self):
        invoice_total = get_invoice_total(self.lines)
        expected = "R2433.40"
        actual = invoice_total
        message = "Unexpected invoice total returned"
        self.assertEqual(expected, actual, message)

    def test_invalid_invoice_total_format_returns_none(self):
        lines = self.lines
        lines[-1] = "Total R24F33.4.0" #Invalid currency format
        invoice_total = get_invoice_total(lines)
        expected = None
        actual = invoice_total
        message = "get_invoice_total() returned something. Expected None"
        self.assertEqual(expected, actual, message)

class GetValueFromCurrencyTestCase(TestCase):
    """ Tests the get_value_from_currency() function in tasks.py ."""

    def setUp(self):
        pass

    def test_valid_currency_string_returns_float(self):
        valid_value = "R123.45"
        expected = 123.45
        actual = get_value_from_currency(valid_value)
        message = "Unexpected value returned from get_value_from_currency()"
        self.assertEqual(expected, actual, message)

    def test_invalid_currency_string_returns_none(self):
        invalid_value = "R$12E.4.5"
        expected = None
        actual = get_value_from_currency(invalid_value)
        message = "get_value_from_currency() returned something. Expected None"
        self.assertEqual(expected, actual, message)

class GetInvoiceSubTotalTestCase(TestCase):
    """ Tests the get_invoice_sub_total() function in tasks.py ."""

    def setUp(self):
        self.lines = get_lines_from_raw_text(RAW_TEXT_FROM_IMAGE_1)

    def test_lines_containing_date_returns_date_invoiced(self):
        invoice_sub_total = get_invoice_sub_total(self.lines)
        expected = "R2116"
        actual = invoice_sub_total
        message = "Unexpected invoice sub total returned"
        self.assertEqual(expected, actual, message)

    def test_invalid_invoice_sub_total_format_returns_none(self):
        lines = self.lines
        lines[-3] = "Subtotal R2.1.1.6" #Invalid currency format
        invoice_sub_total = get_invoice_sub_total(lines)
        expected = None
        actual = invoice_sub_total
        message = "get_invoice_sub_total() returned something. Expected None"
        self.assertEqual(expected, actual, message)

class GetVatPercentageTestCase(TestCase):
    """ Tests the get_vat_percentage() function in tasks.py ."""

    def setUp(self):
        self.lines = get_lines_from_raw_text(RAW_TEXT_FROM_IMAGE_1)

    def test_lines_containing_date_returns_date_invoiced(self):
        vat_percentage = get_vat_percentage(self.lines)
        expected = "15"
        actual = vat_percentage
        message = "Unexpected vat percentage returned"
        self.assertEqual(expected, actual, message)

    def test_invalid_invoice_total_format_returns_none(self):
        lines = self.lines
        lines[-1] = "Total R24F33.4.0" #Invalid currency format
        vat_percentage = get_vat_percentage(lines)
        expected = None
        actual = vat_percentage
        message = "get_vat_percentage() returned something. Expected None"
        self.assertEqual(expected, actual, message)

    def test_invalid_invoice_sub_total_format_returns_none(self):
        lines = self.lines
        lines[-3] = "Subtotal R2.1.1.6" #Invalid currency format
        vat_percentage = get_vat_percentage(lines)
        expected = None
        actual = vat_percentage
        message = "get_vat_percentage() returned something. Expected None"
        self.assertEqual(expected, actual, message)

class GetBilledItemsTestCase(TestCase):
    """ Tests the get_billed_items() function in tasks.py ."""

    def setUp(self):
        self.lines = get_lines_from_raw_text(RAW_TEXT_FROM_IMAGE_1)

    def test_no_billed_items_lines_returns_nothing(self):
        billed_items = get_billed_items(self.lines[:5])
        expected = None
        actual = billed_items
        message = "get_billed_items() returned something. Expected None"
        self.assertEqual(expected, actual, message)

    def test_missing_billed_items_header_returns_nothing(self):
        lines = self.lines
        lines[8] = "Blah dee blah blah"
        billed_items = get_billed_items(lines)
        expected = None
        actual = billed_items
        message = "get_billed_items() returned something. Expected None"
        self.assertEqual(expected, actual, message)

    def test_billed_items_lines_returns_lines(self):
        billed_items = get_billed_items(self.lines)
        expected = [
            "NLP Platform Credits R10 100 R1000",
            "Basic service fee R1000 1 R1000",
            "Invoice Processing R0.01 10000 R100",
            "Policy Documents Processed RO0.02 800 R16",
            ]
        actual = billed_items
        message = "get_billed_items() returned unexpected results."
        self.assertEqual(expected, actual, message)


class GetBilledItemDescriptionTestCase(TestCase):
    """ Tests the get_billed_item_description() function in tasks.py ."""

    def setUp(self):
        lines = get_lines_from_raw_text(RAW_TEXT_FROM_IMAGE_1)
        billed_items = get_billed_items(lines)
        self.billed_item = billed_items[0]

    def test_billed_item_with_too_few_words_returns_nothing(self):
        billed_item = "Too few words"
        expected = None
        actual = get_billed_item_description(billed_item)
        message = "get_billed_item_description() returned something. Expected None"
        self.assertEqual(expected, actual, message)

    def test_billed_item_with_description_returns_description(self):
        expected = "NLP Platform Credits"
        actual = get_billed_item_description(self.billed_item)
        message = "get_billed_item_description() returned unexpected results."
        self.assertEqual(expected, actual, message)


class GetBilledUnitCostTestCase(TestCase):
    """ Tests the get_billed_item_unit_cost() function in tasks.py ."""

    def setUp(self):
        lines = get_lines_from_raw_text(RAW_TEXT_FROM_IMAGE_1)
        billed_items = get_billed_items(lines)
        self.billed_item = billed_items[0]

    def test_billed_item_with_too_few_words_returns_nothing(self):
        billed_item = "Too few words"
        expected = None
        actual = get_billed_item_unit_cost(billed_item)
        message = "get_billed_item_unit_cost() did not return None."
        self.assertEqual(expected, actual, message)

    def test_billed_item_with_non_numeric_unit_cost_returns_nothing(self):
        billed_item = "NLP Platform Credits NonNumericUnitCost! 100 R1000"
        expected = None
        actual = get_billed_item_unit_cost(billed_item)
        message = "get_billed_item_unit_cost() did not return None."
        self.assertEqual(expected, actual, message)

    def test_billed_item_with_unit_cost_returns_unit_cost(self):
        expected = "10"
        actual = get_billed_item_unit_cost(self.billed_item)
        message = "get_billed_item_unit_cost() returned unexpected results."
        self.assertEqual(expected, actual, message)
