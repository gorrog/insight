from django.test import TestCase
from morelia import run
from PIL import Image
from ..models import Invoice
from django.conf import settings
from ..tasks import parse_invoice_image
import time

class ProcessImageTaskFeatureTestCase(TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_launch_task_after_image_upload(self):
        """ Tests that creation of invoices result in image processing tasks. """
        run('invoice_reader/features/process_invoice_image_task.feature',
            self, verbose=True)

    def step_that_we_have_number_sample_invoice_images(self):
        r'that we have 2 sample invoice images'
        image = Image.open('media/tests/invoice1.tiff')
        image.close()
        image = Image.open('media/tests/invoice2.tiff')
        image.close()

    def step_that_the_first_invoice_is_numbered_number(self):
        r'that the first invoice is numbered 1'
        self.image_1 = 'tests/invoice1.tiff'

    def step_that_the_second_invoice_is_numbered_number(self):
        r'that the second invoice is numbered 2'
        self.image_2 = 'tests/invoice2.tiff'

    def step_we_have_a_third_image_containing_random_text(self):
        r'we have a third image containing random text'
        image = Image.open('media/tests/junk.png')
        image.close()
        self.image_3 = 'tests/junk.png'

    def step_we_have_a_fourth_image_containing_no_text(self):
        r'we have a fourth image containing no text'
        image = Image.open('media/tests/no_text.jpg')
        image.close()
        self.image_4 = 'tests/no_text.jpg'

    def step_we_have_an_invoice_object_for_each_image(self):
        r'we have an invoice object for each image'
        self.invoices = {
            "1": Invoice.objects.create(image=self.image_1),
            "2": Invoice.objects.create(image=self.image_2),
            "3": Invoice.objects.create(image=self.image_3),
            "4": Invoice.objects.create(image=self.image_4),
        }

    def step_we_have_a_task_that_can_process_invoices(self):
        r'we have a task that can process invoices'
        self.task = parse_invoice_image

    def step_we_pass_the_id_of_invoice_invoice_number_to_the_task(self, invoice_number):
        r'we pass the id of invoice (.+) to the task'
        self.current_invoice = self.invoices[invoice_number]
        self.job = self.task(self.current_invoice.uuid)

    def step_the_invoice_s_billed_by_address_field_will_have_a_value_of_billed_by_address(self, billed_by_address):
        r'the invoice\'s billed_by_address field will have a value of (.+)'
        time.sleep(2)
        # get the updated invoice
        self.invoice = Invoice.objects.get(uuid=self.current_invoice.uuid)
        self.assertEqual(self.invoice.billed_by_address, billed_by_address)

    def step_the_invoice_s_billed_by_name_field_will_have_a_value_of_billed_by_name(self, billed_by_name):
        r'the invoice\'s billed_by_name field will have a value of (.+)'
        self.assertEqual(self.invoice.billed_by_name, billed_by_name)

    def step_the_invoice_s_billed_by_vat_no_field_will_have_a_value_of_billed_by_vat_no(self, billed_by_vat_no):
        r'the invoice\'s billed_by_vat_no field will have a value of (.+)'
        self.assertEqual(self.invoice.billed_by_vat_no, billed_by_vat_no)

    def step_the_invoice_s_billed_by_tel_no_field_will_have_a_value_of_billed_by_tel_no(self, billed_by_tel_no):
        r'the invoice\'s billed_by_tel_no field will have a value of (.+)'
        self.assertEqual(self.invoice.billed_by_tel_no, billed_by_tel_no)

    def step_the_invoice_s_billed_to_address_field_will_have_a_value_of_billed_to_address(self, billed_to_address):
        r'the invoice\'s billed_to_address field will have a value of (.+)'
        self.assertEqual(self.invoice.billed_to_address, billed_to_address)

    def step_the_invoice_s_billed_to_account_no_field_will_have_a_value_of_billed_to_account_no(self, billed_to_account_no):
        r'the invoice\'s billed_to_account_no field will have a value of (.+)'
        self.assertEqual(self.invoice.billed_to_account_no, billed_to_account_no)

    def step_the_invoice_s_billed_to_name_field_will_have_a_value_of_billed_to_name(self, billed_to_name):
        r'the invoice\'s billed_to_name field will have a value of (.+)'
        self.assertEqual(self.invoice.billed_to_name, billed_to_name)

    def step_the_invoice_s_date_invoiced_field_will_have_a_value_of_date_invoiced(self, date_invoiced):
        r'the invoice\'s date_invoiced field will have a value of (.+)'
        self.assertEqual(self.invoice.date_invoiced, date_invoiced)

    def step_the_invoice_s_invoice_total_field_will_have_a_value_of_invoice_total(self, invoice_total):
        r'the invoice\'s invoice_total field will have a value of (.+)'
        self.assertEqual(self.invoice.invoice_total, invoice_total)

    def step_the_invoice_s_vat_percentage_field_will_have_a_value_of_vat_percentage(self, vat_percentage):
        r'the invoice\'s vat_percentage field will have a value of (.+)'
        self.assertEqual(self.invoice.vat_percentage, vat_percentage)

    def step_the_invoice_s_status_field_will_have_a_value_of_status(self, status):
        r'the invoice\'s status field will have a value of (.+)'
        self.assertEqual(self.invoice.status, status)

