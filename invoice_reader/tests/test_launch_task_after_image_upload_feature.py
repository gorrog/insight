from django.test import TestCase
from morelia import run
from ..models import Invoice
from .helpers import generate_image_with_text
from PIL import Image
import os
import shutil
import django_rq
from django.conf import settings

TEST_IMAGE_DIR = 'test_images/'

class LaunchTaskAfterImageUploadFeatureTestCase(TestCase):
    def setUp(self):
        self.queue = django_rq.get_queue()
        # Empty queue in case there are any tasks left over.
        self.queue.empty()

    def tearDown(self):
        Invoice.objects.all().delete()
        full_image_dir = settings.MEDIA_ROOT + TEST_IMAGE_DIR
        shutil.rmtree(full_image_dir)
        os.mkdir(full_image_dir)
        self.queue.empty()

    def test_launch_task_after_image_upload(self):
        """ Tests that creation of invoices result in image processing tasks. """
        run('invoice_reader/features/launch_task_after_image_upload.feature',
            self, verbose=True)

    def step_that_we_have_a_way_of_generating_test_images(self):
        r'that we have a way of generating test images'
        image_filename = generate_image_with_text(TEST_IMAGE_DIR)
        image_path = settings.MEDIA_ROOT + TEST_IMAGE_DIR + image_filename
        try:
            im=Image.open(image_path)
        except IOError:
            self.fail("Image generator function not working.")

    def step_we_create_num_invoices_invoices_with_our_test_images(self, num_invoices):
        r'we create (.+) invoices with our test images'
        for i in range(int(num_invoices)):
            img_name = TEST_IMAGE_DIR + generate_image_with_text(TEST_IMAGE_DIR)
            invoice = Invoice.objects.create(image=img_name)
        self.assertEqual(str(Invoice.objects.count()), num_invoices)

    def step_num_tasks_tasks_will_be_launched(self, num_tasks):
        r'(.+) tasks will be launched'
        self.assertEqual(str(len(self.queue.jobs)), num_tasks)

    def step_each_invoice_will_have_a_status_of_Queued(self):
        r'each invoice will have a status of \'Queued\''
        total_invoices = Invoice.objects.all().count()
        queued_invoices = Invoice.objects.filter(status=Invoice.QUEUED).count()
        self.assertEqual(total_invoices, queued_invoices)

