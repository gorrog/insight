from PIL import Image, ImageDraw
import random
import uuid
from django.conf import settings

def generate_image_with_text(path=None, width=1500, height=1500, text="Hello World"):
    """ Generates an image with text in a random colour. """
    red_channel_value = random.randint(1,255)
    green_channel_value = random.randint(1,255)
    blue_channel_value = random.randint(1,255)
    color = (red_channel_value, green_channel_value, blue_channel_value)

    img = Image.new('RGB', (width, height), color=color)
    d = ImageDraw.Draw(img)
    d.text((10,10), text, fill=(0,0,0))
    filename = str(uuid.uuid4()) + ".png"
    file_path = settings.MEDIA_ROOT + path + filename
    img.save(file_path)
    return filename
