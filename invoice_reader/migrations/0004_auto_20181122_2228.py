# Generated by Django 2.1.3 on 2018-11-22 22:28

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('invoice_reader', '0003_invoice_status'),
    ]

    operations = [
        migrations.AlterField(
            model_name='invoice',
            name='status',
            field=models.CharField(choices=[('n', 'New'), ('q', 'Queued'), ('d', 'Processed'), ('f', 'Failed')], default='n', max_length=1),
        ),
    ]
