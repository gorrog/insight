from django.db import models
import invoice_reader.tasks as tasks
import django_rq
import uuid
from django.conf import settings

# Create your models here.

class Invoice(models.Model):
    """
    Holds an image file of an invoice and other info about it.

    Initial save will launch a task which will process the image file and fill
    in the metadata attributes if they can be recognised by the OCR software.
    """
    NEW = 'n'
    QUEUED = 'q'
    PROCESSED = 'd'
    FAILED = 'f'
    STATUS_CHOICES = (
        (NEW, 'New'),
        (QUEUED, 'Queued'),
        (PROCESSED, 'Processed'),
        (FAILED, 'Failed'),
    )
    uuid = models.UUIDField(default=uuid.uuid4, editable=False)
    height = models.IntegerField(null=True, blank=True)
    width = models.IntegerField(null=True, blank=True)
    image = models.ImageField(width_field='width', height_field='height')
    billed_by_address = models.CharField(max_length=500, null=True, blank=True)
    billed_by_name = models.CharField(max_length=100, null=True, blank=True)
    billed_by_vat_no = models.CharField(max_length=50, null=True, blank=True)
    billed_by_tel_no = models.CharField(max_length=50, null=True, blank=True)
    billed_to_address = models.CharField(max_length=500, null=True, blank=True)
    billed_to_account_no = models.CharField(max_length=50, null=True, blank=True)
    billed_to_name = models.CharField(max_length=100, null=True, blank=True)
    date_invoiced = models.CharField(max_length=30, null=True, blank=True)
    invoice_total = models.CharField(max_length=30, null=True, blank=True)
    vat_percentage = models.CharField(max_length=30, null=True, blank=True)
    status = models.CharField(max_length=1, choices=STATUS_CHOICES, default=NEW)
    error_message = models.TextField(max_length=255, null=True, blank=True)

    def save(self, *args, **kwargs):
        if not self.id:
            # launch a task to process the image
            queue = django_rq.get_queue(settings.RQ_DEFAULT_QUEUE_NAME)
            queue.enqueue(tasks.parse_invoice_image, self.uuid)
            self.status = self.QUEUED
        super().save(*args, **kwargs)

class BilledItem(models.Model):
    invoice = models.ForeignKey(to=Invoice, on_delete=models.CASCADE)
    description = models.CharField(max_length=200)
    unit_cost = models.CharField(max_length=30, null=True, blank=True)
    qty_h_rate = models.CharField(max_length=30, null=True, blank=True)
    amount = models.CharField(max_length=30, null=True, blank=True)
