from django.contrib import admin
from .models import Invoice, BilledItem

# Register your models here.

class BilledItemInline(admin.TabularInline):
    model = BilledItem

class InvoiceAdmin(admin.ModelAdmin):
    inlines = [
        BilledItemInline,
    ]


admin.site.register(Invoice, InvoiceAdmin)
