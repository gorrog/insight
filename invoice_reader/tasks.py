from PIL import Image
import pytesseract
import invoice_reader.models
import contextlib
import time
from django.utils.dateparse import parse_datetime
import contextlib
from dateutil.parser import parse
import invoice_reader.models

# Config options can be found with the command 'tesseract --help-extra
# Explanation of current config:
# --psm 4 : treat image as a single column of text
# --oem 1 : use the neural net OCR engine
# --dpi 300 : image resolution is 300 dpi
TESSERACT_CONFIG = '--psm 4 --oem 1 --dpi 300'

def parse_invoice_image(uuid):
    """Uses OCR to extract text from an invoice image"""
    # First, wait a few seconds to ensure that the invoice has been written to
    # the DB.
    MISSING_FIELD_TEXT = "Cannot find field"
    time.sleep(1)
    invoice = invoice_reader.models.Invoice.objects.get(uuid=uuid)
    image = Image.open(invoice.image)
    raw_text_from_image = pytesseract.image_to_string(image,
                                                      config=TESSERACT_CONFIG)
    lines = get_lines_from_raw_text(raw_text_from_image)
    if lines:
        if len(lines) > 12:
            invoice.billed_by_address = (get_billed_by_address(lines) or
                                                MISSING_FIELD_TEXT)
            invoice.billed_by_name = get_billed_by_name(lines) or MISSING_FIELD_TEXT
            invoice.billed_by_vat_no = get_vat_no(lines) or MISSING_FIELD_TEXT
            invoice.billed_by_tel_no = (get_billed_by_tel_no(lines) or
                                        MISSING_FIELD_TEXT)
            invoice.billed_to_address = (get_billed_to_address(lines) or
                                        MISSING_FIELD_TEXT)
            invoice.billed_to_account_no = (get_billed_to_account_no(lines) or
                                        MISSING_FIELD_TEXT)
            invoice.billed_to_name = (get_billed_to_name(lines) or
                                        MISSING_FIELD_TEXT)
            invoice.date_invoiced = (get_date_invoiced(lines) or
                                        MISSING_FIELD_TEXT)
            invoice.invoice_total = (get_invoice_total(lines) or
                                        MISSING_FIELD_TEXT)
            invoice.vat_percentage = (get_vat_percentage(lines) or
                                        MISSING_FIELD_TEXT)
            billed_items = get_billed_items(lines)
            add_billed_items_to_invoice(invoice.id, billed_items)
            invoice.status = invoice_reader.models.Invoice.PROCESSED
        else:
            invoice.error_message = "Less than 13 lines of text found in image. Cannot process invoice."
            invoice.status = invoice_reader.models.Invoice.FAILED
    else:
        invoice.error_message = "Image does not appear to contain any text. Cannot process invoice."
        invoice.status = invoice_reader.models.Invoice.FAILED
    invoice.save()

def get_lines_from_raw_text(raw_text_from_image):
    """Removes empty lines and whitespace from lines."""
    # Split on line endings and remove leading a trailing whitespace
    raw_lines = [line.strip() for line in raw_text_from_image.split('\n')]
    # Get rid of empty lines
    clean_lines = [line for line in raw_lines if len(line) > 0]
    return clean_lines

def get_vat_no(lines):
    """Attempts to find a vat no in a list of lines."""
    vat_line = lines[1]
    words = vat_line.split()
    for word in words:
        with contextlib.suppress(ValueError):
            if type(int(word)) == int:
                vat_no = int(word)
                return vat_no

def get_billed_by_email(lines):
    """Attempts to find a billed by email address in a list of lines."""
    email_line = lines[0]
    words = email_line.split()
    for word in words:
        if '@' in word:
            billed_by_email = word
            return billed_by_email

def get_billed_by_address(lines):
    """
    Attempts to find a billed by address in a list of lines.

    Constructs this from a combination of the part after the billed by email
    and the part after the vat no.
    """
    billed_by_email = get_billed_by_email(lines)
    vat_no = get_vat_no(lines)
    if billed_by_email and vat_no:
        first_part = lines[0].split(billed_by_email)[1].strip()
        if first_part:
            second_part = lines[1].split(str(vat_no))[1].strip()
            if second_part:
                billed_by_address = first_part + ", " + second_part
                return billed_by_address

def get_billed_by_name(lines):
    """Attempts to find a billed by email address in a list of lines."""
    name_line = lines[0]
    email = get_billed_by_email(lines)
    if email:
        name = name_line.split(email)[0].strip()
        return name


def get_billed_by_tel_no(lines):
    """Attempts to find a billed by tel no in a list of lines."""
    tel_no_line = lines[2]
    if 'Tel No:' in tel_no_line:
        right_part = tel_no_line.split('Tel No:')[1]
        if right_part:
            tel_no = right_part.split()[0].strip()
            return tel_no


def get_billed_to_address(lines):
    """Attempts to find a billed to street address in a list of lines."""
    address_line = lines[6]
    if 'Date Of Issue' in address_line:
        address = address_line.split('Date Of Issue')[0].strip()
        return address

def get_billed_to_account_no(lines):
    """Attempts to find a billed to street address in a list of lines."""
    account_line = lines[3]
    if 'Billed To' in account_line:
        right_part = account_line.split('Billed To')[1]
        if 'Invoice' in right_part:
            left_part = right_part.split('Invoice')
            if len(left_part) > 1:
                billed_to_account_no = left_part[0].strip()
                return billed_to_account_no

def get_billed_to_name(lines):
    """Attempts to find a billed to name in a list of lines."""
    name_line = lines[4]
    if 'Client Name' in name_line:
        right_part = name_line.split('Client Name')[1]
        words = right_part.split()
        if len(words) > 1:
            billed_to_name = words[0].strip()
            return billed_to_name

def get_date_invoiced(lines):
    """Attempts to find an invoiced date in a list of lines."""
    date_line = lines[7]
    date = date_line.strip()
    with contextlib.suppress(ValueError):
        parse(date)
        return date # Return original string if it's a date

def get_invoice_total(lines):
    """Attempts to find an invoice total in a list of lines."""
    invoice_total_line = lines[-1]
    if 'Total' in invoice_total_line:
        total = invoice_total_line.split('Total')[1].strip()
        number = get_value_from_currency(total)
        if number:
            return total # Return original string if valid

def get_value_from_currency(currency_string):
    """Attempts convert a currency string of format R1234.56 into a float."""
    number_string = currency_string[1:]
    with contextlib.suppress(ValueError):
        number = float(number_string)
        return number

def get_invoice_sub_total(lines):
    """Attempts to find an invoice total in a list of lines."""
    invoice_total_line = lines[-3]
    if 'Subtotal' in invoice_total_line:
        sub_total = invoice_total_line.split('Subtotal')[1].strip()
        number = get_value_from_currency(sub_total)
        if number:
            return sub_total # Return original string if valid

def get_vat_percentage(lines):
    total = get_invoice_total(lines)
    sub_total = get_invoice_sub_total(lines)
    if total and sub_total:
        total_value = get_value_from_currency(total)
        sub_total_value = get_value_from_currency(sub_total)
        if total_value and sub_total_value:
            vat_percentage = str(int(((total_value/sub_total_value)-1)*100))
            return vat_percentage

def get_billed_items(lines):
    BILLED_BY_HEADER_WORDS = [
        'Description', 'Unit', 'Cost', 'Qty/', 'Hr', 'Rate', 'Amount'
    ]
    if lines:
        if len(lines) > 12:
            billed_by_header = lines[8]
            valid_header = False
            # Check that we have at least one expected word in the header line.
            for word in BILLED_BY_HEADER_WORDS:
                if word in billed_by_header and not valid_header:
                    valid_header = True
            if valid_header:
                return lines[9:-3]

def get_billed_item_description(billed_item):
    words = billed_item.split()
    if len(words) > 3:
        return " ".join(words[:-3])

def get_billed_item_unit_cost(billed_item):
    words = billed_item.split()
    unit_cost = None
    if len(words) > 3:
        unit_cost_raw = words[-3]
        unit_cost = get_value_from_currency(unit_cost_raw)
        if unit_cost:
            unit_cost = str(int(unit_cost))
    return unit_cost


# FIXME: add tests for this (missing due to time constraints)
def add_billed_items_to_invoice(invoice_id, billed_items):
    invoice = invoice_reader.models.Invoice.objects.get(id=invoice_id)
    for billed_item in billed_items:
        kwargs = {}
        kwargs["invoice"] = invoice
        kwargs["description"] = get_billed_item_description(billed_item)
        kwargs["unit_cost"] = get_billed_item_unit_cost(billed_item)
        invoice_reader.models.BilledItem.objects.create(**kwargs)
